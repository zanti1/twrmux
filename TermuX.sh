#mode by Mr. Zanti -X
red='\e[0;31m' # Red
green='\e[0;32m' # Green
yellow='\e[0;33m' # Yellow
blue='\e[0;34m' # Blue
purple='\e[0;35m' # Purple
cyan='\e[0;36m' # Cyan
white='\e[0;37m' # White
sleep 1
pkg install termux-api -y
clear
echo -e $cyan "start.... \n"
sleep 0.20
banner(){
echo -en  $green'
mmmmmmm        mmmmmm        mmmmm         m    m        m    m        m    m
   #           #             #   "#        ##  ##        #    #         #  #
   #           #mmmmm        #mmmm"        # ## #        #    #          ##
   #           #             #   "m        # "" #        #    #         m""m
   #           #mmmmm        #    "        #    #        "mmmm"        m"  "m

https://github.com/zanti1/Twrmux
mode by Mr. Z
'
}
banner 
line(){
echo -en $red '
[ 00 ] clear-Dasplay
[  1 ] termux-reast
[  2 ] open-url
[  3 ] termux-wake-lock
[  4 ] termux-torch
[  5 ] Phone-info
[  6 ] termux-wifi-connectioninfo
[  7 ] termux-vibrate
[  8 ] termux-tts-speak
[  9 ] termux-wake-unlock
[ 10 ] termux-wifi-enable
[ 12 ] termux-speech-to-text
[ 13 ] termux-setup-storage
[ 14 ] termux-battery-status
[ 15 ] termux-camera-info
[ 16 ] termux-volume
[ 17 ] termux-telephony-call
[ 18 ] termux-sms-list
[ 19 ] termux-contact-list
[ 20 ] termux-telephony-deviceinfo
[ 21 ] termux-fingerprint
[ 22 ] termux-wifi-scaninfo
[ 23 ] termux-sensor
[ 24 ] check_youtube-video
[ 99 ] update
'
}
line
read -p "enter " AA
case $AA in
00)
  echo "clear-Dasplay "
  clear
 ;;
1)
  echo "teemux-reset "
  termux-reset
 ;;
2)
  read -p "open-url > " A1
  termux-open-url $A1
 ;;
3)
  echo "termux-wake-lock"
  termux-wake-lock
 ;;
4)
  echo "Usage: termux-torch [on | off] "
  read -p "enter >" A2
  termux-torch $A2
 ;;
5)
  echo "Phone-info"
  termux-info
 ;;
6)
  echo "termux-wifi-connectioninfo"
  termux-wifi-connectioninfo
 ;;
7)
  echo "termux-vibrate "
  termux-vibrate
 ;;
8)
  echo "termux-tts-speak "
  termux-tts-speak
 ;;
9)
  echo "termux-wake-unlock "
  termux-wake-unlock
 ;;
10)
  read -p "termux-wifi-enable [true | false] " A3
  termux-wifi-enable $A3
 ;;
12)
  echo "termux-speech-to-text "
  termux-speech-to-text
 ;;
13)
  echo "termux-setup-storage "
  termux-setup-storage
 ;;
14)
  echo "termux-battery-status "
  termux-battery-status
 ;;
15)
  echo "termux-camera-info "
  termux-camera-info
 ;;
16)
  echo "termux-volume "
  termux-volume
 ;;
17)
  echo "termux-telephony-call "
read -p "enter PH-number  " A4
  termux-telephony-call $A4
 ;;
18)
  echo "termux-sms-list "
  termux-sms-list
 ;;
19)
  echo "termux-contact-list "
  termux-contact-list
 ;;
20)
  echo "termux-telephony-deviceinfo "
  termux-telephony-deviceinfo
 ;;
21)
  echo "termux-fingerprint "
  termux-fingerprint
 ;;
22)
  echo "termux-wifi-scaninfo "
  termux-wifi-scaninfo
 ;;
23)
   echo "termux-sensor "
  termux-sensor -l
 ;;
24)
  echo "start.... "
  termux-open-url https://youtu.be/VSZEbFAW8S0
 ;;
99)
  echo -en $yellow "update"
  pkg install git -y
  git clone https://github.com/zanti1/Twrmux
  mv Twrmux /$HONE/
  clear
  ;;

esac
